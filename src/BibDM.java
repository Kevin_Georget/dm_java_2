import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer minus = null;
        for (Integer elem : liste){
            if (minus == null){
                minus = elem;
                }
            if (elem < minus){
                minus = elem;
            }
        }
        return minus;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        List<T> newListe = new ArrayList<>(liste);
        newListe.add(valeur);
        Collections.sort(newListe);

        if (newListe.get(0) == valeur){ return true;}
        return false;
    }


    /**
     * Dictionnaire d'une liste et d'un dico
     * @param dicoElem un dictionnaire de liste 
     * @param liste une liste triée
     * @return un dictionnaire avec ses anciens élements et les éléments de la liste en plus
     */
    public static<T extends Comparable<? super T>> Map<T, Integer> dicoList(Map<T,Integer> dElem, List<T> liste){
        for (T elem : liste){
            if(dElem.containsKey(elem)){
                dElem.put(elem,dElem.get(elem)+1);
            }
            else {
                dElem.put(elem,1);
            }
        }
        return dElem;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        Map<T,Integer> dicoElem = new HashMap<T, Integer>();
        BibDM.dicoList(dicoElem, liste1);
        BibDM.dicoList(dicoElem, liste2);
        List<T> liste = new ArrayList<>();
        for(T cle : dicoElem.keySet()){
            if(dicoElem.get(cle)> 1){
                liste.add(cle);
            }
        }
        Collections.sort(liste);
        return liste;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        char caractere;
        String mot = "";
        List<String> res = new ArrayList<>();
        
        for (int i=0; i<texte.length(); i++){
            caractere = texte.charAt(i);
            if (caractere == ' '){
                if (mot != ""){ 
                    res.add(mot);
                    mot = "";
                }
                
            }
            else{
                mot+= caractere;
            }
        }
        if ( texte.length()>0 && mot!= ""){res.add(mot);}
        return res;
    }

    /**
     * Renvoie un dictionnaire dont les clé sont les mot et les clé leur nombre d'occurence
     * @param texte une chaine de caractères
     * @return un dictionnaire
     */
    public static Map<String, Integer> dico(List<String> liste){
        Map<String, Integer> dicoMot;
        dicoMot = new HashMap<String, Integer>();
        for (String mot : liste){
            if(dicoMot.containsKey(mot)){
                dicoMot.put(mot,dicoMot.get(mot)+1);
            }
            else {
                dicoMot.put(mot,1);
            }
        }
        return dicoMot;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */
    public static String motMajoritaire(String texte){
        if(texte.length() == 0){
            return null;
        }
        List<String> lmot = new ArrayList<>();
        lmot = BibDM.decoupe(texte);
        Map<String,Integer> dicoMot = new HashMap<String, Integer>();
        dicoMot = BibDM.dico(lmot);
        String resultat = "";
        int compteur = 0;
        for(String cle : dicoMot.keySet()){
            if (dicoMot.get(cle)> compteur){
                resultat = cle;
                compteur =  dicoMot.get(cle);
            }
            if (dicoMot.get(cle)== compteur && resultat.compareTo(cle)>0){
                resultat = cle;
            }
        }
        return resultat;
    }

    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int cpt = 0;
        char caractere;
        for (int i=0; i<chaine.length(); i++){
            caractere = chaine.charAt(i);
            if (caractere == '('){
                cpt+=1;
            }
            else{
                if(caractere == ')'){
                    cpt -= 1; 
                }
            }
            if(cpt < 0){ return false;}
        }
        
        if (cpt != 0){return false;}
        else{return true;}  
    }

    /**
     * Permet de tester si une chaine est bien crocheté
     * @param chaine une chaine de caractères composée de [ et de ]
     * @return true si la chaine est bien crocheté et faux sinon. Par exemple []] est mal crocheté et [[]][] est bien crocheté.
     */
    public static boolean bienCrochets(String chaine){
        int cpt = 0;
        char caractere;
        for (int i=0; i<chaine.length(); i++){
            caractere = chaine.charAt(i);
            if (caractere == '['){
                cpt+=1;
            }
            else{
                if(caractere == ']'){
                    cpt -= 1; 
                }
            }
            if(cpt < 0){ return false;}
        }
        
        if (cpt != 0){return false;}
        else{return true;}  
    }

    /**
     * Permet de tester si une chaine n'a pas de problème avec l'ordre et que chaque crochet / parenthèse s'ouvre hors de parenthèse / crochet
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors que ([]) est bien parenthèsée.
     */
    public static boolean bienPositionné(String chaine){
        char prec = ' ';
        char caractere;
        for (int i=0; i<chaine.length(); i++){
                caractere = chaine.charAt(i);
                if(prec == ' '){ 
                    prec = chaine.charAt(i);
                    }
                else{
                    if (prec == '(' && chaine.charAt(i) == ']' || prec == '[' && chaine.charAt(i) == ')'){
                        return false;
                    }
                }
            }
        return true;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( de  ) de [ et de ]
     * @return true si la chaine est bien parentèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors que ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        
        if (chaine.length() == 0){return true;}
        else{
            if(bienParenthesee(chaine) && bienCrochets(chaine) && bienPositionné(chaine) ){return true;}
        }
        return false;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if(liste.isEmpty()){
            return false;
        }
        else{
            int debut = 0;
            int fin = liste.size()-1;
            while(debut<fin){
                int milieu = (debut+fin)/2;
                if(liste.get(milieu)<valeur){
                    debut = milieu+1;
                }
                else {
                    fin = milieu;
                }
            }
            return debut<liste.size() && valeur == liste.get(debut);
        }
    }

}
